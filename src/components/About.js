import Accordion from 'react-responsive-accordion';

export default React => props => {
  return (<div className="about section center"  id="about">
    <h3 className="title">Acerca de Nosotros</h3>

    <div className="content">
      <img src="/build/images/photo.jpg" title="" className="liquid-image" />

    <div className="row">
      <div className="col">
        <div className="inner quote">
          <div className="text">Ofrecemos asesoría técnica en el cultivo del nogal, proporciona insumos, servicios de poda y fumigación, recolección semi-mecánica de la cosecha de nuez y procesamiento de nuez para la venta con cáscara. A través de la prestación de nuestros servicios hemos logrado impactar positivamente en los costos de producción, estableciendo economías de escala en la adquisición de insumos, en el uso de maquinaria y equipo especializado para la producción y cosecha.</div>
        </div>
      </div>
      <div className="col col-2">
        <div className="inner">
          <Accordion
            transitionTime={100}>
            <div data-trigger="Historia">
              <p>Alrededor de los años 50’s se establecieron los primeros nogales en la costa de Hermosillo, en el campo propiedad de los hermanos miguel y Luis Quiroz frener esta vendría a ser la primera huerta. Posteriormente en los años 70’s y visualizando el campo en los patrones de cultivo tradicionales y viendo el posible potencial de la fruta y cultura, fue así como los señores José García de la Garza, Carlos Baranzini y Enrique Mazón López iniciaron su plantaciones de nogal de una manera más extensiva en la región. Es también en estos años donde arranca la organización de productores de nuez, primeramente constituida bajo el nombre de asociación agrícola local de productores de nuez de la costa de Hermosillo. En los años de los 80’s se constituye Productora de Nuez SPR de RI con el establecimiento de la primer planta beneficiadora de nuez del estado de sonora. Es también a finales de esta década empieza el despunte más importante del nogal pecanero en la región. La nuez pecanera cultivada en la costa de Hermosillo sonora se ha comercializado en los últimos 20 años directamente con clientes nacionales e internacionales.</p>
            </div>

            <div data-trigger="Misión">
              <p>Somos una empresa de productores orientada al desarrollo de servicios que mejoren la productividad en las plantaciones de nuez pecanera, así como la competitividad de nuestros productos a través de una mayor integración de todos los participantes en el Sistema Producto, participamos en la generación de proyectos de inversión, el desarrollo tecnológico y la presencia en los mercados con productos diferenciados y de gran calidad.</p>
            </div>

            <div data-trigger="Visión">
              <p>Ser la organización de servicios, procesos y comercialización de nuez que funcione como vínculo entre productores y clientes, contribuyendo en el crecimiento del sistema productivo y comercial de la nuez, para que sea una actividad competitiva y autosustentable.</p>
            </div>
          </Accordion>
        </div>
      </div>
    </div>
    </div>
  </div>);
}
