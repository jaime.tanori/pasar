export default React => props => {
  return (<header>
    <nav>
      <a href="#" title="" className="logo">
        <img src="/build/images/logo.png" alt="pasar" className="logo" />
      </a>
      <ul>
        <li><a href="#home" title="">Inicio</a></li>
        <li><a href="#about" title="">Acerca de Nosotros</a></li>
        <li><a href="#services" title="">Servicios</a></li>
        <li><a href="#products" title="">Productos</a></li>
        <li><a href="#contact" title="">contacto</a></li>
      </ul>
    </nav>
  </header>);
}
