import React, {Component} from 'react';

import createHeader from './components/Header';
import createHome from './components/Home';
import createAbout from './components/About';
import createServices from './components/Services';
import createProducts from './components/Products';
import Contact from './components/Contact';
import createFooter from './components/Footer';
import createMap from './components/Map';

let Header = createHeader(React);
let Home = createHome(React);
let About = createAbout(React);
let Services = createServices(React);
let Products = createProducts(React);
let Footer = createFooter(React);
let Map = createMap(React);

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Home />
        <About />
        <Services />
        <Products />
        <div className="video">
          <a href="https://www.youtube.com/watch?v=3cIEx9QyIP0" title="" target="_blank">
            <i className="icon-play2" />
          </a>
        </div>
        <Contact />
        <Map
          center={{lat: 29.075590, lng: -111.079558}}
          zoom={14}
          language="es"/>
        <Footer />
      </div>
    );
  }
}
export default App;
