export default React => props => {
  return (<div className="home section"  id="home">
    <img src="/build/images/logo.png" alt="" className="logo" />
    <h1>Productora de Nuez</h1>
    <h2>La calidad es el resultado de todos nuestros esfuerzos</h2>
    <a href="#about" className="btn">Conocenos</a>
  </div>);
}
