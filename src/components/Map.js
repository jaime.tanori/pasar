import GoogleMapReact from 'google-map-react';

export default React => props => {
  const Icon = () => (
    <i className="icon-office map-icon"></i>
  )

  function createMapOptions(maps) {
    return {
      panControl: false,
      mapTypeControl: false,
      scrollwheel: false,
      gestureHandling: 'cooperative',
      styles: [{ stylers: [{ 'saturation': -100 }, { 'gamma': 0.8 }, { 'lightness': 4 }, { 'visibility': 'on' }] }]
    }
  }

  return (
    <div className="map">
      <GoogleMapReact
        defaultCenter={props.center}
        defaultZoom={props.zoom}
        options={createMapOptions}
      >
        <Icon
          lat={props.center.lat}
          lng={props.center.lng}
        />
      </GoogleMapReact>
    </div>
  );
}
