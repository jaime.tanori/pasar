export default React => props => {
  return (<footer>
    <div>&copy; 2011 Productora de Nuez SPR de RI 2017. Hermosillo, Sonora, México.</div>
    <ul className="menu light">
      <li><a href="#"><i className="icon-facebook"></i></a></li>
      <li><a href="#"><i className="icon-twitter"></i></a></li>
      <li><a href="#"><i className="icon-mail"></i></a></li>
    </ul>
  </footer>);
}
