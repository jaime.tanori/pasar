import React from 'react';
import PropTypes from 'prop-types';
import Spinner from './Spinner';

class Contact extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      phone: '',
      subject: '',
      message: '',
      loading: false
    };

    this.baseState = this.state;
  }

  handleChange(event) {
    if(this.state.hasOwnProperty(event.target.name)) {
      this.setState({[event.target.name]: event.target.value});
    }
  }

  handleSubmit(event) {
    let { name, email, subject, phone, message } = this.state;

    event.preventDefault();
    this.setState({loading: true});

    emailjs
      .send(
        'default_service',
        'nuevo_mensaje_en_su_pagina_web',
        {
          from_name: name,
          from_email: email,
          message_html: `Email: ${email}\tPhone: ${phone}\tSubject: ${subject}\t\t${message}`
        }
      )
      .then(this.onComplete.bind(this), this.onError.bind(this));
  }

  onComplete(response) {
    this.setState({
      loading: false
    });
    this.resetForm();
    alert('Su mensaje se ha enviado, gracias.');
  }

  resetForm() {
    this.setState(this.baseState);
  }

  onError(error) {
    this.setState({
      loading: false,
      error: error
    });

    alert('Error: ' + error.message + '\nPor favor intente de nuevo.');
  }

  isValid() {
    var { name, email, phone, message } = this.state;

    if(name.length && email.length && phone.length && message.length) {
      return true;
    }

    return false;
  }

  render() {
    let { name, email, subject, phone, message, loading } = this.state;
    let isValid = this.isValid();

    return (<div className="contact section center"  id="contact">
      <h3 className="title">Contacto</h3>

      <div className="content">
        <div className="row">
          <form
            className="col col-2"
            onSubmit={(event) => this.handleSubmit(event)}
            disabled={loading}>
            <div className="padded">
              {loading && <Spinner />}
              <div className="row">
                <div className="col">
                  <input type="text" name="name" placeholder="Nombre" value={name} required onChange={(event) => this.handleChange(event)}/>
                </div>
                <div className="col">
                  <input type="email" name="email" placeholder="Email" value={email} required onChange={(event) => this.handleChange(event)} />
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <input type="tel" name="phone" placeholder="Telefono" value={phone} required onChange={(event) => this.handleChange(event)} />
                </div>
                <div className="col">
                  <input type="text" name="subject" placeholder="Asunto" value={subject} onChange={(event) => this.handleChange(event)} />
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <textarea placeholder="Comentarios" name="message" value={message} required onChange={(event) => this.handleChange(event)}></textarea>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  {isValid && <button type="submit" className="btn">Enviar</button>}
                  {!isValid && <button type="button" className="btn" disabled>Enviar</button>}
                </div>
              </div>
            </div>
          </form>
          <div className="col">
            <div className="padded">
              <h4>Dirección</h4>
              <p><strong>Productora de Nuez SPR de RI</strong> Carretera a Bahia de Kino, Kilometro 12.6 Col. La Manga, C.P. 83220, Hermosillo, Sonora</p>
              <div>
                <p><strong>T:</strong> +52-662-261-0035</p>
                <p><strong>C:</strong> correo@email.com</p>
                <p><strong>H:</strong> Lunes a Viernes de 9AM a 5PM, Sabados de 9AM a 2PM</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
};


export default Contact;
