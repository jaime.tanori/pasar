export default React => props => {
  return (<div className="services section center light"  id="services">
    <h3 className="title title-2">Nuestros Servicios</h3>

    <div className="content">
      <div className="row">
        <div className="col">
          <div className="block">
            <h4 className="title"><i className="icon-scissors"/> Poda Selectiva</h4>
            <p>Se utiliza para controlar el tamaño de los árboles reduciendo el volumen de la copa del árbol e incrementando la entrada de luz dentro de la copa del árbol, induciendo la fructificación en áreas sombreadas no productivas. La penetración de luz dentro del árbol y entre árboles se limita debido al excesivo tamaño y densidad foliar de la copa. La falta de penetración de luz en estas huertas adultas limita el crecimiento durante la estación y la producción. También afecta negativamente la calidad de la nuez y contribuye al incremento de la alternancia a través de los años.</p>
          </div>
        </div>
        <div className="col">
          <div className="block">
            <h4 className="title"><i className="icon-gears"/> Poda Mecanica</h4>
            <p>Al ser una poda mecanizada es indiscriminada y el corte es por donde pasan las cuchillas. Esta práctica no selectiva es más económica, rápida y soluciona problemas de huertas cerradas, permite una mejor aspersión de insecticidas y fertilizantes foliares, por otro lado, reduce la alternancia e incrementa el tamaño de la nuez, también aumenta el porcentaje de almendra al mejorar la proporción peso/almendra. Además de la poca selección, otra desventaja que se presenta es que se tiene que emplear mano de obra para sacar de los huertos la madera cortada, lo cual implica un costo elevado.</p>
          </div>
        </div>
        <div className="col">
          <div className="block">
            <h4 className="title"><i className="icon-rays"/> Vibrado</h4>
            <p>Es el sistema utilizado en plantaciones intensivas, donde se provoca la caída de los frutos por medio de vibradores que según el tamaño de los árboles serán de inercia (agarre al tronco y utilizable en árboles no muy grandes) o de sirga (agarre a las ramas y utilizable en árboles más grandes). De forma alternativa se utilizan vibradores telescópicos que son enormemente versátiles, lo que les permite el acceso tanto a ramas como a troncos. Su gran inconveniente en la actualidad es el elevado precio. El vibrado mecánico se utiliza de forma mayoritaria en plantaciones modernas de nogal. Los frutos pueden caer directamente en el suelo o en toldos, al estilo del olivar y del almendro.</p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <div className="block">
            <h4 className="title"><i className="icon-factory"/> Maquila</h4>
            <p>La productora cuenta con: maquinaria muy especializada en el nogal: aspersora, podadora, sacudidores o vibradores, barredoras y cosechadoras; una planta beneficiadora de nuez, para la limpieza, selección y envasado de nueces con cáscara; una bodega en la Central de Abastos de la ciudad de México. Es socia de la empresa Agroprocesamientos de Sonora (en la cual participan 18 productores y Productora de Nuez), que trabaja en el quebrado y dándole valor agregado a la nuez: garapiñado en varias modalidades, nuez con chocolate en varios tipos y nuez tostada con sal.</p>
          </div>
        </div>
        <div className="col">
          <div className="block">
            <h4 className="title"><i className="icon-water"/> Aspersión</h4>
            <p>La actual tecnología de riego consiste en la aplicación del riego en base a demandas reales (de la planta) con sistemas de goteo enterrado y con sensores de humedad que permiten ajustar la programación. Los sensores de humedad se colocan en el suelo y miden la disponibilidad de agua en el suelo, indicando así los requerimientos reales de la planta. El cultivo no requiere o demanda tanto líquido durante las etapas de brotación ni cosecha, por lo que se asegura el programa de riego en términos de que sea justo con la planta y atienda sus necesidades específicas. De esta manera se ha pasado de un manejo convencional de riego por inundación a riego presurizado con sensores. La tecnología implica el conocimiento del funcionamiento de sensores, del sistema de riego por goteo y de la fisiología de la planta.</p>
          </div>
        </div>
        <div className="col">
          <div className="block">
            <h4 className="title"><i className="icon-lab"/> Agroquimicos</h4>
            <p>La tendencia de la aplicación del agua de riego es hacia la eliminación de las pérdidas de agua por conducción y aprovechar el agua como medio para la aplicación de fertilizantes y otros agroquímicos (Mejoradores de suelos, insecticidas, fungicidas y lavados de suelo).  La cianamida de hidrógeno, es un agroquímico de uso común y hasta la fecha es la tecnología más efectiva para inducir la brotación aún en años de baja producción. Se utiliza como compensador de frío, aplicada mediante la aspersión foliar, elimina uno de los “caminos de respiración”, provocando que la planta sufra un “estrés” anaeroóbico y se propicie entonces la brotación, incrementando la fructificación femenina.</p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col"></div>
        <div className="col">
          <div className="block">
            <h4 className="title"><i className="icon-headphones"/> Asesoria Tecnica</h4>
            <p>En relación con el desarrollo de la actividad productiva del nogal, creamos esquemas de asistencia técnica también noverdosos, contamos con técnicos especializados en el manejo del Nogal, situación que abre las puertas a la consulta y acceso a capacitación e innovación especializada. En el proceso productivo de la nuez la asistencia técnica ha sido un factor decisivo para elevar la productividad. A través de los técnicos se lleva tecnología a los productores en cada fase de éste, y en base a las condiciones específicas del suelo, la raíz y las hojas, se toman decisiones sobre los productos que deben aplicarse y las labores a realizar.</p>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  </div>);
}
