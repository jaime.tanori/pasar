export default React => props => {
  return (<div className="products section center" id="products">
    <h3 className="title">Productos</h3>

    <div className="content">
      <div className="row">
        <div className="col col-a">
          <div className="block">
            <h3>Wichita</h3>
            <div>
              <p>Originada mediante cruza controlada (‘Halbert’ x ‘Mahan’) hecha por LD ROmberg, del Servicio de</p>
              <p>Investigación en Agricultura del Departamento de Agricultura de Estado Unidos, en 1940.</p>
              <p>Fue liberada como variedad en 1959.</p>
              <p>La nuez es oblonga, con ápice agudo a puntiagudo y asimétrico y la parte basal redondeada.</p>
              <p>El conteo unitario de nueces por libra es de 43</p>
              <p>El rendimiento de carne es de 62%</p>
              <p>El color de la carne es dorado a café claro con ranuras angostas y un amplio, superficial hendedura basal.</p>
              <p>Es un cultivar precoz y muy productivo.</p>
              <p>Madura en la mitad de la temporada, junto con ‘Western’, de 4 a 20 días después de ‘Stuart’, dependiendo de la localidad (es más temprana en el Sureste)</p>
              <p>El árbol es moderadamente erecto, vigoroso, frecuentemente con un crecimiento retrasado.</p>
              <p>Recomendado para Arizona, Arkansas, California, Nuevo México, Oklahoma y Texas.</p>
            </div>
          </div>
        </div>
        <div className="col col-b">
          <div className="block">
            <h3>Mahan</h3>
            <div>
              <p>Selección realizada por M. Chestnutt en 1910. Los derechos de propagación se vendieron a FA</p>
              <p>Mahan, Monticello Nursery Co, en Monticello, Florida, en 1927.</p>
              <p>La nuez es oblonga, con ápice y base agudo.</p>
              <p>La nuez es frecuentemente asimétrica.</p>
              <p>Da la apariencia de pinchada en la parte central debido al alisamiento de las superficies abaxial y adaxial.</p>
              <p>Aplanada en un corte seccional.</p>
              <p>Presenta 32 nueces por libra.</p>
              <p>El rendimiento de almendra es de 58%.</p>
              <p>La almendra tiene canaletas dorsales y una hendidura basal, frecuentemente pobremente llena en la base, se siente como madera en textura.</p>
              <p>Tendencia a liberar polen de la mitad a la parte tardía de la temporada de polinización con una receptividad de estigma de mediados de temporada.</p>
              <p>Es un árbol precoz y muy productivo, con fuerte tendencia a sobrecargarse de nuez en árboles maduros.</p>
              <p>Madura 12 días antes que Stuart.</p>
              <p>Mahan es la mamá de Tejas, Kiowas, Harper, Mahan-Stuart, Maramec; y papá de Choctaw, Wichita y Mohawk.</p>
              <p>Recomendada para su plantación en Arkansas.</p>
            </div>
          </div>
        </div>
        <div className="col col-c">
          <div className="block">
            <h3>Western Schley (Western)</h3>
            <div>
              <p>Selección realizada por E.E. Riesen, en San Saba, Texas en 1924</p>
              <p>Se le llamó ‘Schley’ por su gran parecido a este cultivar. Alguna vez se pensó que estaba emparentado con la variedad ‘San Saba’, pero no se pudo confirmar mediante pruebas moleculares.</p>
              <p>Hay más árboles de ‘Western’ que de cualquier otra variedad en el Oeste.</p>
              <p>La nuez es elíptica oblonga a oblonga, con un ápice anguloso y una base aguda, es asimétrica.</p>
              <p>Redondeada en un corte seccional. La superficie de la cáscara es rugosa.</p>
              <p>Presenta 57 nueces por libra.</p>
              <p>El rendimiento de carne (almendra) es de 58%.</p>
              <p>El color de la almendra es de dorado a café claro, con canales dorsales que atrapan partes de laparte interior de la cáscara y provocan la quebradura de las almendras durante el descascarado.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>);
}
